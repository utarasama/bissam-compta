﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encaissements
{
    public class Employee
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public uint CP { get; set; }
        public string Ville { get; set; }
        public ulong NumSecu { get; set; }
        public string Emploi { get; set; }
        public string Mail { get; set; }
        public byte Classification { get; set; }
        public string Anciennete { get; set; }

        /// <summary>
        /// Constructeur généré par Visual Studio
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="adresse"></param>
        /// <param name="cP"></param>
        /// <param name="ville"></param>
        /// <param name="numSecu"></param>
        /// <param name="emploi"></param>
        /// <param name="mail"></param>
        /// <param name="classification"></param>
        /// <param name="anciennete"></param>
        public Employee(string nom, string prenom, string adresse, uint cP, string ville, ulong numSecu, string emploi, string mail, byte classification, string anciennete)
        {
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            CP = cP;
            Ville = ville;
            NumSecu = numSecu;
            Emploi = emploi;
            Mail = mail;
            Classification = classification;
            Anciennete = anciennete;
        }

        public override string ToString()
            => return $"{Nom} {Prenom}";
    }
}
