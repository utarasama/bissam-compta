﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Encaissements
{
    public static class Employees
    {
        /// <summary>
        /// La liste de tous les employés
        /// </summary>
        public static List<Employee> Liste { get; private set; }

        /// <summary>
        /// Recherche un employé à l'aide de son nom
        /// </summary>
        /// <param name="p_nom">Le nom de l'employé</param>
        /// <returns>Retourne une instance de la classe <c>Employee</c> correspondant à la recherche</returns>
        public static List<Employee> SearchByName(string p_nom)
        {
            List<Employee> v_list = new List<Employee>();
            foreach (Employee v_employee in Liste)
                if (v_employee.Nom == p_nom)
                    v_list.Add(v_employee);
            return v_list;
        }

        /// <summary>
        /// Recherche un employé à l'aide de son nom et de son prénom
        /// </summary>
        /// <param name="p_nom">Le nom de l'employé</param>
        /// <param name="p_prenom">Le prénom de l'employé</param>
        /// <returns>Retourne une instance de la classe <c>Employee</c> correspondant à la recherche</returns>
        public static Employee SearchByName(string p_nom, string p_prenom)
        {
            foreach (Employee v_employee in Liste)
                if (v_employee.Nom == p_nom && v_employee.Prenom == p_prenom)
                    return v_employee;
            return null;
        }

        /// <summary>
        /// Recherche un employé à l'aide de son prénom
        /// </summary>
        /// <param name="p_nom">Le prénom de l'employé</param>
        /// <returns>Retourne une instance de la classe <c>Employee</c> correspondant à la recherche</returns>
        public static List<Employee> SearchByFirstName(string p_prenom)
        {
            List<Employee> v_list = new List<Employee>();
            foreach (Employee v_employee in Liste)
                if (v_employee.Prenom == p_prenom)
                    v_list.Add(v_employee);
            return v_list;
        }

        /// <summary>
        /// Ajoute un employé à la liste
        /// </summary>
        /// <param name="p_employee">L'employé à ajouter</param>
        public static void Add(Employee p_employee)
        {
            Liste.Add(p_employee);
            Save();
        }

        /// <summary>
        /// Supprime un employé de la liste
        /// </summary>
        /// <param name="p_employee">L'employé à supprimer</param>
        public static void Remove(Employee p_employee)
        {
            Liste.Remove(p_employee);
            Save();
        }

        /// <summary>
        /// Méthode qui génère une fiche de paie
        /// </summary>
        /// <param name="p_employee">L'employé pour lequel la fiche sera générée</param>
        public static void GeneratePaySlip(Employee p_employee)
        {
            // Méthode pour générer une fiche de paie
        }

        /// <summary>
        /// Enregistre les employés au format JSON
        /// </summary>
        public static void Save()
        {
            string v_serializedEmployees = JsonConvert.SerializeObject(Liste);
            File.WriteAllText(@".\SerializedObjects\Employees.json", EncryptionHelper.Encrypt(v_serializedEmployees));
        }

        /// <summary>
        /// Charge en mémoire la liste de tous les employés au format JSON
        /// </summary>
        public static void Load()
        {
            string v_serializedEmployees = File.ReadAllText(@".\SerializedObjects\Employees.json");
            List<Employee> employees = JsonConvert.DeserializeObject<List<Employee>>(EncryptionHelper.Decrypt(v_serializedEmployees));
            Liste = new List<Employee>();
            if (employees != null)
                Liste = employees;
        }
    }
}
