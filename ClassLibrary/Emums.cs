﻿namespace Encaissements
{
    public enum Argent
    {
        Entree,
        Sortie
    }

    public enum Paiement
    {
        Espèces,
        Chèque,
        CB,
        Virement,
        NULL
    }
}
