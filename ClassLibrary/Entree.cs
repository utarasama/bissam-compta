﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Encaissements
{
    public class Entree
    {
        public string Date { get; set; }
        public string Heure { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public Paiement ModeDePaiement { get; set; }
        public float Montant { get; set; }
        public float Paied { get; set; }

        public Entree(string date, string heure, string nom, string description, Paiement modeDePaiement, float montant, float paied)
        {
            Date = date;
            Heure = heure;
            Nom = nom;
            Description = description;
            ModeDePaiement = modeDePaiement;
            Montant = montant;
            Paied = paied;
        }


        /// <summary>
        /// Génère la somme de toutes les entrées
        /// </summary>
        /// <param name="entrees">La liste des entrées</param>
        /// <returns>Retourne la somme de toute les entrées</returns>
        public static float Total(List<Entree> entrees)
        {
            float v_somme = 0;
            foreach (Entree entree in entrees)
                v_somme += entree.Montant;
            return v_somme;
        }

        /// <summary>
        /// Enregistre les entrées dans le disque dur
        /// </summary>
        /// <param name="entrees">La liste des entrées à enregistrer</param>
        public static void Save(List<Entree> entrees)
        {
            string v_entreesString = JsonConvert.SerializeObject(entrees, Formatting.Indented);
            v_entreesString = EncryptionHelper.Encrypt(v_entreesString);
            File.WriteAllText(@".\SerializedObjects\Entrees.json", v_entreesString);
        }

        /// <summary>
        /// Charge les entrées enregistrées
        /// </summary>
        /// <returns>La liste des entrées enregistrées</returns>
        public static List<Entree> Load()
        {
            string v_entreesString = File.ReadAllText(@".\SerializedObjects\Entrees.json");
            v_entreesString = EncryptionHelper.Decrypt(v_entreesString); // Déchiffrement

            // JSON -> List<Entree>
            List<Entree> entrees = JsonConvert.DeserializeObject<List<Entree>>(v_entreesString);
            if (entrees != null)
                return JsonConvert.DeserializeObject<List<Entree>>(v_entreesString);
            return new List<Entree>();
        }
    }
}
