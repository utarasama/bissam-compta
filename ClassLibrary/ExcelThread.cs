﻿using GemBox.Spreadsheet;
using System.Diagnostics;

namespace Encaissements
{
    class ExcelThread
    {
        private string SalaireBrut { get; set; }
        private bool? CheckboxIsChecked { get; set; }
        private string DateDebut { get; set; }
        private string DateFin { get; set; }
        private Employee Employé { get; set; }
        private Argent SelectedMode { get; set; }

        public ExcelThread(Employee p_employé, string p_salaireBrut, bool? p_chkbxChecked, string p_dateDebut, string p_dateFin)
        {
            Employé = p_employé;
            SalaireBrut = p_salaireBrut;
            CheckboxIsChecked = p_chkbxChecked;
            DateDebut = p_dateDebut;
            DateFin = p_dateFin;
        }

        public ExcelThread(Argent p_encaissement)
        {
            SelectedMode = p_encaissement;
        }

        public void GeneratePaySlip()
        {
            // If using Professional version, put your serial key below.
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            string v_excelFilePath = @".\Fiche de paie";

            // Ouverture du fichier Excel pour modification

            ExcelFile v_workbook = ExcelFile.Load(v_excelFilePath + ".xlsx");
            ExcelWorksheet v_worksheet = v_workbook.Worksheets.ActiveWorksheet;
            var v_headersFooters = v_worksheet.HeadersFooters;

            v_worksheet.Cells[8, 1].Value = SalaireBrut; // On entre le salaire brut dans la fiche de paie
            v_worksheet.Cells[0, 3].Value = DateDebut;
            v_worksheet.Cells[0, 5].Value = DateFin;
            for (byte i = 13; i <= 43; i++)
                for (byte j = 1; j <= 5; j++)
                    v_worksheet.Cells[i, j].Calculate();

            v_headersFooters.DefaultPage = null;
            v_headersFooters.FirstPage = null;
            v_headersFooters.EvenPage = null;

            // On modifie l'échelle du document
            // Pour que cela rentre dans une feuille A4
            v_worksheet.PrintOptions.FitWorksheetWidthToPages = 1;
            v_worksheet.PrintOptions.FitWorksheetHeightToPages = 1;

            // Enregistrement du document en PDF
            v_workbook.Save(@".\GeneratedFiles\Fiche de paie.pdf");


            if (CheckboxIsChecked == true)
                Process.Start(@".\GeneratedFiles\Fiche de paie_" + Employé + "_" + DateFin + ".pdf"); // On l'ouvre avec le programme par défaut
        }

        public void GenerateCollectionTable()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");


        }
    }
}
