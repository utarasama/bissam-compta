﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Encaissements
{
    public class Sortie
    {
        public string Date { get; set; }
        public string Heure { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public Paiement ModeDePaiement { get; set; }
        public float Montant { get; set; }

        public Sortie(string date, string heure, string nom, string description, Paiement modeDePaiement, float montant)
        {
            Date = date;
            Heure = heure;
            Nom = nom;
            Description = description;
            ModeDePaiement = modeDePaiement;
            Montant = montant;
        }


        /// <summary>
        /// Génère la somme de toutes les sorties
        /// </summary>
        /// <param name="entrees">La liste des sorties</param>
        /// <returns>Retourne la somme de toute les sorties</returns>
        public static float Total(List<Sortie> sorties)
        {
            float v_somme = 0;
            foreach (Sortie sortie in sorties)
                v_somme += sortie.Montant;
            return v_somme;
        }

        /// <summary>
        /// Enregistre les sorties sur le disque dur
        /// </summary>
        /// <param name="sorties">La liste des sorties à enregistrer</param>
        public static void Save(List<Sortie> sorties)
        {
            string v_sortiesString = JsonConvert.SerializeObject(sorties, Formatting.Indented);
            v_sortiesString = EncryptionHelper.Encrypt(v_sortiesString);
            File.WriteAllText(@".\SerializedObjects\Sorties.json", v_sortiesString);
        }

        /// <summary>
        /// Charge les sorties enregistrées
        /// </summary>
        /// <returns>La liste des sorties entrgistrées</returns>
        public static List<Sortie> Load()
        {
            string v_sorrtiesString = File.ReadAllText(@".\SerializedObjects\Entrees.json");
            v_sorrtiesString = EncryptionHelper.Decrypt(v_sorrtiesString); // Déchiffrement

            // JSON -> List<Sortie>
            List<Sortie> sorties = JsonConvert.DeserializeObject<List<Sortie>>(v_sorrtiesString);
            if (sorties != null)
                return JsonConvert.DeserializeObject<List<Sortie>>(v_sorrtiesString);
            return new List<Sortie>();
        }
    }
}
