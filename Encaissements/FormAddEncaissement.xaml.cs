﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Encaissements
{
    /// <summary>
    /// Logique d'interaction pour FormAddEncaissement.xaml
    /// </summary>
    public partial class FormAddEncaissement : Page
    {
        /// <summary>
        /// Les booléens de validation du formulaire
        /// </summary>
        private bool IsDateOK, IsHeureOK, IsNameOK, IsDescOK, IsPaymentOK, IsMontantOK, IsPaiedMontantOK;
        /// <summary>
        /// Mode d'encaissement choisi (entrée ou sortie)
        /// </summary>
        private Argent argent;
        public FormAddEncaissement(Argent p_modeDePaiement)
        {
            InitializeComponent();
            argent = p_modeDePaiement;
            IsDateOK = false;
            IsHeureOK = false;
            IsNameOK = false;
            IsDescOK = false;
            IsMontantOK = false;
            IsPaiedMontantOK = false;
            if (p_modeDePaiement == Argent.Entree)
                Titre.Text = "Ajouter une entrée";
            else
            {
                IsPaiedMontantOK = true;
                Titre.Text = "Ajouter une sortie";
                montantPayé.Visibility = Visibility.Hidden;
            }

            date.SelectedDate = DateTime.Now; // Affichage de la date du jour
            heure.SelectedTime = DateTime.Now;
            IsDateOK = true;
            IsHeureOK = true;
        }

        private void Description_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsDescOK = Regex.Match(
                description.Text,
                "^([A-Za-z0-9àáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸ]+( )*)+$").Success;
            ValidateForm();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsPaymentOK = true;
            ValidateForm();
        }

        private void Montant_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsMontantOK = Regex.Match(montant.Text, "^[0-9]+((.|,)[0-9]{1,2})?$").Success;
            ValidateForm();
        }

        private void Heure_SelectedTimeChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            IsHeureOK = heure.SelectedTime != null;
            ValidateForm();
        }

        private void MontantPayé_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsPaiedMontantOK = Regex.Match(montantPayé.Text, "^[0-9]+((.|,)[0-9]{1,2})?$").Success;
            ValidateForm();
        }

        private void Date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            IsDateOK = date.SelectedDate != null;
            ValidateForm();
        }

        private void Nom_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsNameOK = Regex.Match(
                nom.Text,
                "^([A-Za-z0-9àáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸ]+( )*)+$").Success;
            ValidateForm();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            Paiement paiement = Paiement.NULL;
            // On détermine quel mode de paiement on a sélectionné
            switch (ModeDePaiement.SelectedIndex)
            {
                case 0:
                    paiement = Paiement.Espèces;
                    break;
                case 1:
                    paiement = Paiement.Chèque;
                    break;
                case 2:
                    paiement = Paiement.CB;
                    break;
                case 3:
                    paiement = Paiement.Virement;
                    break;
            }

            App.Current.MainWindow.Content = new Tableaux();

            // On instancie l'objet correspondant (Entrée ou Sortie)
            if (argent == Argent.Entree)
                Tableaux.Ajouter(new Entree(
                    date.SelectedDate.Value.ToShortDateString(),
                    heure.SelectedTime.Value.ToShortTimeString(),
                    nom.Text,
                    description.Text,
                    paiement,
                    float.Parse(montant.Text),
                    float.Parse(montantPayé.Text)));
            else
                Tableaux.Ajouter(new Sortie(
                    date.SelectedDate.Value.ToShortDateString(),
                    heure.SelectedTime.Value.ToShortTimeString(),
                    nom.Text,
                    description.Text,
                    paiement,
                    float.Parse(montant.Text)));
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
            => App.Current.MainWindow.Content = new Tableaux();

        /// <summary>
        /// Méthode qui valide la conformité du formulaire pour soumission
        /// </summary>
        private void ValidateForm()
            => ButtonOK.IsEnabled = IsDateOK && IsHeureOK && IsNameOK && IsDescOK && IsPaymentOK && IsPaiedMontantOK && IsMontantOK;
    }
}
