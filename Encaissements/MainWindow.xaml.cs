﻿using System.IO;
using System.Windows;

namespace Encaissements
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // On vérifie si le dossier existe
            if (!Directory.Exists(@".\SerializedObjects"))
                Directory.CreateDirectory(@".\SerializedObjects");

            // On vérifie si les fichiers existent
            if (!File.Exists(@".\SerializedObjects\Entrees.json"))
                File.Create(@".\SerializedObjects\Entrees.json");

            if (!File.Exists(@".\SerializedObjects\Sorties.json"))
                File.Create(@".\SerializedObjects\Sorties.json");

            Main.Content = new Tableaux();
        }
    }
}
