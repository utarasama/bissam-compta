﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Encaissements
{
    /// <summary>
    /// Logique d'interaction pour Encaissements.xaml
    /// </summary>
    public partial class Tableaux : Page
    {
        public static List<Entree> Entrees { get; private set; }
        public static List<Sortie> Sorties { get; private set; }
        private static Entree SelectedEntree { get; set; }
        private static Sortie SelectedSortie { get; set; }
        public Tableaux()
        {
            InitializeComponent();

            Entrees = Entree.Load();
            Sorties = Sortie.Load();

            // On définit la source de données sur les listes pour qu'elles soient mises à jour automatiquement
            ListeEntrees.ItemsSource = Entrees;
            ListeSorties.ItemsSource = Sorties;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            Argent v_argent = Input.IsSelected ? Argent.Entree : Argent.Sortie;
            // On se dirige vers le formulaire d'ajout d'une entrée ou d'une sortie
            App.Current.MainWindow.Content = new FormAddEncaissement(v_argent);
        }

        /// <summary>
        /// Affiche le montant total des entrées quand on sélectionne le bon onglet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Input_Selected(object sender, RoutedEventArgs e)
        {
            montantTotal.Text = $"Montant total des entrées : {Entree.Total(Entrees)} €";
            if (Button_Delete.IsEnabled)
                Button_Delete.IsEnabled = false;
            SelectedSortie = null;
            ListeSorties.SelectedItem = null;
        }

        /// <summary>
        /// Affiche le montant total des sorties quand on sélectionne le bon onglet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Output_Selected(object sender, RoutedEventArgs e)
        {
            montantTotal.Text = $"Montant total des sorties : {Sortie.Total(Sorties)} €";
            if (Button_Delete.IsEnabled)
                Button_Delete.IsEnabled = false;
            SelectedEntree = null;
            ListeEntrees.SelectedItem = null;
        }

        /// <summary>
        /// Enregistre les encaissements
        /// </summary>
        public static void Save()
        {
            Entree.Save(Entrees);
            Sortie.Save(Sorties);
        }

        /// <summary>
        /// Ajout d'une entrée
        /// </summary>
        /// <param name="p_entree">L'entrée à ajouter</param>
        public static void Ajouter(Entree p_entree)
            => Entrees.Add(p_entree);

        /// <summary>
        /// Ajout d'une sortie
        /// </summary>
        /// <param name="p_sortie">Sortie à ajouter</param>
        public static void Ajouter(Sortie p_sortie)
            => Sorties.Add(p_sortie);

        /// <summary>
        /// Suppression d'une entrée
        /// </summary>
        /// <param name="p_sortie">Entrée à supprimer</param>
        private void Supprimer(Entree p_entree)
            => Entrees.Remove(p_entree);

        /// <summary>
        /// Suppression d'une sortie
        /// </summary>
        /// <param name="p_sortie">Sortie à supprimer</param>
        private void Supprimer(Sortie p_entree)
            => Sorties.Remove(p_entree);

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            SelectedSortie = null;
            SelectedEntree = null;

            ListeEntrees.Items.Refresh();
            ListeSorties.Items.Refresh(); // Mets à jour l'UI
            Save();

            // Maj des totaux
            if (Input.IsSelected)
                Input_Selected(null, null);
            else
                Output_Selected(null, null);

            ListeEntrees.SelectedItem = null;
            ListeSorties.SelectedItem = null;
        }

        private void ListeSorties_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if ((sender as ListView).SelectedItem != null)
            {
                Button_Delete.IsEnabled = true;
                SelectedSortie = (Sortie)(sender as ListView).SelectedItem;
            }
        }

        private void ListeEntrees_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if ((sender as ListView).SelectedItem != null)
            {
                Button_Delete.IsEnabled = true;
                SelectedEntree = (Entree)(sender as ListView).SelectedItem;
            }
        }

        private void Button_Delete_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedSortie != null || SelectedEntree != null)
            {
                if (SelectedEntree != null)
                {
                    MessageBoxResult v_result = MessageBox.Show(StringOfMessageBox(SelectedEntree),
                                                    "Supprimer une entrée",
                                                    MessageBoxButton.YesNo,
                                                    MessageBoxImage.Question,
                                                    MessageBoxResult.No);
                    if (v_result == MessageBoxResult.Yes)
                        Supprimer(SelectedEntree);
                }
                else
                {
                    MessageBoxResult v_result = MessageBox.Show(StringOfMessageBox(SelectedSortie),
                                                    "Supprimer une sortie",
                                                    MessageBoxButton.YesNo,
                                                    MessageBoxImage.Question,
                                                    MessageBoxResult.No);
                    if (v_result == MessageBoxResult.Yes)
                        Supprimer(SelectedSortie);
                }

                Page_Loaded(null, null);
            }
        }

        private string StringOfMessageBox(Entree p_entree)
            => $"Voulez-vous vraiment supprimer cette ligne datant du {p_entree.Date} à {p_entree.Heure} au nom de {p_entree.Nom} pour un montant de {p_entree.Montant} € ? Cette action est irréversible.";

        private string StringOfMessageBox(Sortie p_sortie)
            => $"Voulez-vous vraiment supprimer cette ligne datant du {p_sortie.Date} à {p_sortie.Heure} au nom de {p_sortie.Nom} pour un montant de {p_sortie.Montant} € ? Cette action est irréversible.";
    }
}
