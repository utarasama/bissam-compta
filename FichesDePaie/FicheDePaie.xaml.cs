﻿using Encaissements;
using System.Windows;
using System.Windows.Controls;

namespace FichesDePaie
{
    /// <summary>
    /// Logique d'interaction pour FichesDePaie.xaml
    /// </summary>
    public partial class FicheDePaie : Page
    {
        public FicheDePaie()
        {
            InitializeComponent();
            Employees.Load();
            listOfEmployees.ItemsSource = Employees.Liste;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
            => App.Current.MainWindow.Content = new FormEmployee();

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
