﻿using System.Windows.Controls;

namespace FichesDePaie
{
    /// <summary>
    /// Logique d'interaction pour FormFicheDePaie.xaml
    /// </summary>
    public partial class FormEmployee : Page
    {
        public FormEmployee()
        {
            InitializeComponent();
        }

        private void ButtonCancel_Click(object sender, System.Windows.RoutedEventArgs e)
            => App.Current.MainWindow.Content = new FicheDePaie();

        private void ButtonOK_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}
