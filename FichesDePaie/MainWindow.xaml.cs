﻿using System.IO;
using System.Windows;

namespace FichesDePaie
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // On vérifie si le dossier existe
            if (!Directory.Exists(@".\SerializedObjects"))
                Directory.CreateDirectory(@".\SerializedObjects");

            if (!File.Exists(@".\SerializedObjects\Employees.json"))
                File.Create(@".\SerializedObjects\Employees.json");

            Main.Content = new FicheDePaie();
        }
    }
}
